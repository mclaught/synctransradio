/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainThread.h
 * Author: user
 *
 * Created on 4 сентября 2020 г., 14:32
 */

#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include "config.h"
#include "MainThreadSource.h"
#include "MainThreadDest.h"
#include "UARTThread.h"
#include "UDPOutThread.h"
#include "UARTThreadWPi.h"

#if (IN_CH == 0)
    extern MainThreadSource mainThrd;
#else
    extern MainThreadDest mainThrd;
#endif
    
#if (OUT_CH == 0)
    extern UARTThread outThrd;
#elif (OUT_CH == 1)
    extern UDPOutThread outThrd;
#elif (OUT_CH == 2)
    extern UARTThreadWPi outThrd;
#else    
    #error "OUT_CH undefined"
#endif

#endif /* MAINTHREAD_H */

