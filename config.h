/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   config.h
 * Author: user
 *
 * Created on 16 января 2020 г., 18:49
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <opus/opus.h>

#define VERSION "20.09.11-02"

#define MAX_PACKET_SIZE_UDP 1024

#define CHANNELS 1
#define FRAME_DURATION 10
#define OPUS_FRAMESIZE  OPUS_FRAMESIZE_10_MS 
#define SAMPLE_RATE_IN 44100
#define SAMPLE_RATE_OPUS 48000
#define MAX_BITRATE 40000
#define WORKER_THREADS  8
#define MAX_CHANNELS    8

#define FRAME_SIZE_IN (SAMPLE_RATE_IN * FRAME_DURATION / 1000)
#define FRAME_SIZE_OPUS (SAMPLE_RATE_OPUS * FRAME_DURATION / 1000)
#define PACKET_SIZE (MAX_BITRATE * FRAME_DURATION /8000)

//#define IN_CH 1   //0 - Local, 1 - Remote
//#define OUT_CH 0   //0 - UART, 1 - UDP, 2 - wiringPi, 

#endif /* CONFIG_H */

