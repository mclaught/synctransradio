/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThread.cpp
 * Author: user
 * 
 * Created on 17 января 2020 г., 14:07
 */
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <map>
#include <sched.h>

#include "UARTThread.h"
#include "Channel.h"
#include "Settings.h"
#include "Monitor.h"

UARTThread::UARTThread() : BaseOutThread() {
}

UARTThread::UARTThread(const UARTThread& orig) {
}

UARTThread::~UARTThread() {
    close(fd);
}

bool UARTThread::init(){
    string portname = settings.getStr("uart_port", "/dev/ttyAMA0");
    cout  << "UART port " << portname << "...";
    
    fd = open (portname.c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd < 0)
    {
            cerr << "error " << errno << " opening " << portname << " " << strerror (errno) << endl;
            return false;
    }

    if(!set_interface_attribs (fd, B460800, 0) < 0)  // set speed to 115,200 bps, 8n1 (no parity)
        return false;
    cout  << "460800 8n1...";
    
    if(!set_blocking (fd, 0))
        return false;
    cout  << "opened" << endl;
    
    send_setts();
    
    return true;
}

bool UARTThread::set_interface_attribs (int fd, int speed, int parity)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
            cerr << "error " << errno << " from tcgetattr" << endl;
            return false;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
                                    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
            cerr << "error " << errno << " from tcsetattr" << endl;
            return false;
    }
    return true;
}

bool UARTThread::set_blocking (int fd, int should_block)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
            cerr << "error " << errno << " from tcgetattr" << endl;
            return false;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        cerr << "error " << errno << " from tcsetattr: " << strerror(errno) << endl;
        return false;
    }
    
    return true;
}


int UARTThread::write_fd(const void* buf, size_t sz){
//    const char* str = reinterpret_cast<const char*>(buf);
//    int wr = 0;
//    for(int i=0; i<sz; i++){
//        wr += write(fd, &str[i], 1);
//    }
//    return wr;
    
    int wr = write(fd, buf, sz);
    if(wr == -1){
        cerr << "UARTThread write: " << strerror(errno) << endl;
        return -1;
    }
    
    monitor.add_uart_traf(wr*8);
    
    return wr;
}