/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ChannelWorker.cpp
 * Author: user
 * 
 * Created on 16 января 2020 г., 18:37
 */

#include <unistd.h>
#include <iostream>

#include "ChannelWorker.h"
#include "Monitor.h"

void print_affinity(const char* thrd);

ChannelWorker::ChannelWorker() {
}

ChannelWorker::ChannelWorker(const ChannelWorker& orig) {
}

ChannelWorker::~ChannelWorker() {
}

void ChannelWorker::start(){
    MyThread::start();
//    set_prio(100);
}

void ChannelWorker::terminate(){
    unlock();
    MyThread::terminate();
}

void ChannelWorker::clear(){
    lock_guard<mutex> lock(mtx);
    channels.clear();
}

void ChannelWorker::add_channel(PChannel ch){
    {
        lock_guard<mutex> lock(mtx);
        channels.insert(ch);
    }
}

int ChannelWorker::count(){
    return channels.size();
}

int ChannelWorker::set_complexity(){
    int c = channels.size() < 3 ? 10 : 4;
    for(auto ch : channels){
        ch->set_complexity(c);
    }
    return c;
}

bool ChannelWorker::data_ready(){
    for(PChannel ch : channels){
        if(ch->data_ready())
            return true;
    }
    return false;
}

void ChannelWorker::unlock(){
    unique_lock<mutex> sync_lock(sync_mtx);
    sync.notify_one();
}

bool ChannelWorker::process(){
    bool processed = false;
    for(PChannel ch : channels){
        if(ch->process())
            processed = true;
    }
    return processed;
}

void ChannelWorker::exec(){
    if (sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1) {
        perror("sched_setaffinity");
    }
//    print_affinity("worker");
    
    while(active){
        unique_lock<mutex> sync_lock(sync_mtx);
        sync.wait(sync_lock);
        
        bool processed = false;
        {
            lock_guard<mutex> lock(mtx);
            processed = process();
        }
        
//        if(!processed)
//            usleep(1000);
    }
    printf("Worker %d stoped\r\n", index);
}

