/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   myip.h
 * Author: user
 *
 * Created on 15 ноября 2017 г., 16:02
 */

#ifndef MYIP_H
#define MYIP_H

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push,1)    
    
struct MyIP
{
    uint8_t IHL:4;
    uint8_t version:4;
    uint8_t dif_services;
    uint16_t length;
    uint16_t ident;
    uint16_t offset:13;
    uint8_t flags:3;
    uint8_t TTL;
    uint8_t protocol;
    uint16_t hdr_crc;
    uint32_t src_ip;
    uint32_t dst_ip;
};

struct MyUDP
{
    uint16_t src_port;
    uint16_t dst_port;
    uint16_t udp_len;
    uint16_t udp_crc;
};
    
#pragma pack(pop)    


#ifdef __cplusplus
}
#endif

#endif /* MYIP_H */

