/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Monitor.cpp
 * Author: user
 * 
 * Created on 28 января 2020 г., 0:24
 */

#include "Monitor.h"

#include <sys/times.h>
#include <unistd.h>
#include <iostream>

using namespace std;

Monitor monitor;

Monitor::Monitor() {
    uint32_t tps = sysconf(_SC_CLK_TCK);
    if(tps)
        mspc = 1000/tps;
}

Monitor::Monitor(const Monitor& orig) {
}

Monitor::~Monitor() {
}

void Monitor::add_opus(int v){
    lock_guard<mutex> lock(mtx);
    
    opus_sum += v;
    opus_n++;
}

void Monitor::add_uart(int v){
    lock_guard<mutex> lock(mtx);
    
    if(v > uart_max)
        uart_max = v;
}

void Monitor::add_net_traf(int v){
    lock_guard<mutex> lock(mtx);
    
    net_traf += v;
}

void Monitor::add_uart_traf(int v){
    lock_guard<mutex> lock(mtx);
    
    uart_traf += v;
}

void Monitor::set_opus_tm(){
    opus_tm += delay_us();
    opus_tm_n++;
}

void Monitor::set_sound(int16_t v){
    if(v > sound_max)
        sound_max = v;
    if(v < sound_min)
        sound_min = v;
}

void Monitor::show(){
    lock_guard<mutex> lock(mtx);
    
    uint32_t tm = get_tick_count();
    if((tm - last_tm) >= 1000){// && opus_n && opus_tm_n
        //cout << opus_sum/opus_n << "\t" << net_traf << "\t" << uart_traf << endl;
        printf("SOUND: %d\tUART:%d\r\n", sound_max-sound_min, uart_traf);
        last_tm = tm;
        opus_sum = opus_n = net_traf = uart_traf = opus_tm = opus_tm_n = sound_max = sound_min = 0;
    }
}

uint32_t Monitor::get_tick_count(){
    tms tm;
    return times(&tm)*mspc;//tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

void Monitor::clear(){
    opus_sum = opus_n = uart_max = 0;
}

void Monitor::pinpoint(){
    gettimeofday(&tv_start, nullptr);
}

uint32_t Monitor::delay_us(){
    timeval tv;
    gettimeofday(&tv, nullptr);
    
    uint32_t delta = (tv.tv_sec - tv_start.tv_sec)*1000000 + (tv.tv_usec - tv_start.tv_usec);
    
    return delta;
}