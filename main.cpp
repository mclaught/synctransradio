/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: user
 *
 * Created on 16 января 2020 г., 13:30
 */

#include <cstdlib>
#include <opus/opus.h>
#include <iostream>
#include <syslog.h>
#include <sys/time.h>
#include <sys/epoll.h>
#include <sys/stat.h>
#include <memory>
#include <signal.h>
#include <execinfo.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <arpa/inet.h>
#include <map>
#include <unistd.h>
#include <mutex>
#include <fstream>
#include <sched.h>

#include "config.h"
#include "myip.h"
#include "Channel.h"
#include "ChannelWorker.h"
#include "UARTThread.h"
#include "UARTThreadWPi.h"
#include "Settings.h"
#include "Monitor.h"
#include "ChannelUARTWorker.h"
#include "BaseOutThread.h"
#include "UDPOutThread.h"
#include "MainThread.h"

#define PIDFILE "/run/synctransradio"

using namespace std;

#if (IN_CH == 0)
    MainThreadSource mainThrd;
#else
    MainThreadDest mainThrd;
#endif

#if (OUT_CH == 0)
    UARTThread outThrd;
#elif (OUT_CH == 1)    
    UDPOutThread outThrd;
#elif (OUT_CH == 2)    
    UARTThreadWPi outThrd;
#else
    #error "OUT_CH undefined"
#endif
    
int ConfigureSignalHandlers();
void log_error(string str);

bool is_daemon_runing(){
    ifstream test(PIDFILE);
    return test.good();
    
//    cout << "pidof " << system("pidof synctransradio") << endl;
}

void run(){
    cout << "Starting..." << endl;
    ConfigureSignalHandlers();
    
    openlog("SynctransRadio", LOG_PID | LOG_CONS | LOG_NDELAY | LOG_NOWAIT, LOG_LOCAL0);
    
    mainThrd.run();
}

/*
 * 
 */
int main(int argc, char** argv) {
    if(argc == 1){
        if(!is_daemon_runing()){
            printf("synctransradio v.%s\n\n", VERSION);
            run();
        }else{
            cout << "Не возможно запустить в режиме приложения при запущенной службе" << endl;
        }
        return 0;
    }else if(argc == 2){
        string cmd = string(argv[1]);
        if(cmd == "start"){
            if(is_daemon_runing()){
                printf("Already running\r\n");
                exit(1);
            }
            
            auto pid = fork();
            if(pid == -1){
                cerr << strerror(errno) << endl;
                return -1;
            }else if(pid == 0){
                printf("Starting service...\r\n");
                chdir("/");
                close(STDIN_FILENO);
                close(STDOUT_FILENO);
                close(STDERR_FILENO);
                run();
                return 0;
            }else{
                ofstream pidfile;
                pidfile.open(PIDFILE);
                pidfile << pid;
                pidfile.close();
                return 0;
            }
        }else if(cmd == "stop"){
            if(!is_daemon_runing()){
                printf("Is not running\r\n");
                exit(1);
            }
            
            ifstream pidfile;
            pidfile.open(PIDFILE);
            if(pidfile.is_open()){
                int pid = 0;
                pidfile >> pid;
                pidfile.close();

                kill(pid, SIGTERM);
                
                unlink(PIDFILE);
            }
        }else if(cmd == "pair"){
        }else if(cmd == "-v"){
            printf("%s", VERSION);
#if (IN_CH == 0)
            printf("-lan");
#elif (IN_CH == 1)
            printf("-wan");
#endif
#if (OUT_CH == 0)
            printf("-uart");
#elif (OUT_CH == 1)
            printf("-wan");
#endif
            printf("\n");
        }
    }else{
        cerr << "Invalid arguments amount" << endl;
        return 1;
    }

    return 0;
}


static void signal_error(int sig, siginfo_t *si, void *ptr)
{
/*
    void* ErrorAddr;
    void* Trace[16];
    int x;
    int TraceSize;
    char** Messages;
    
    // запишем в лог что за сигнал пришел


#if __WORDSIZE == 64 // если дело имеем с 64 битной ОС
    // получим адрес инструкции которая вызвала ошибку
    cerr << "Signal: " << strsignal(sig) << ", Addr: 0x" << hex << (uint64_t) si->si_addr << endl;
    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%lu\n", strsignal(sig), (uint64_t)si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_RIP];
#else 
    // получим адрес инструкции которая вызвала ошибку
    syslog(LOG_LOCAL0 | LOG_ERR, "Signal: %s, Addr: 0x%X\n", strsignal(sig), (int) si->si_addr);
    ErrorAddr = (void*) ((ucontext_t*) ptr)->uc_mcontext.gregs[REG_EIP];
#endif

    // произведем backtrace чтобы получить весь стек вызовов 
    TraceSize = backtrace(Trace, 16);
    Trace[1] = ErrorAddr;

    // получим расшифровку трасировки
    Messages = backtrace_symbols(Trace, TraceSize);
    if (Messages)
    {
        log_error("== Backtrace ==");

        // запишем в лог
        for (x = 1; x < TraceSize; x++)
        {
            log_error(Messages[x]);
        }

        log_error("== End Backtrace ==");
        free(Messages);
    }

    closelog();
    _exit(0);
*/
}

void TermHandler(int sig)
{
    cerr << "caught TERM signal: " << strsignal(sig) << endl;
    mainThrd.stop();
}

void HupHandler(int sig)
{
    cerr << "caught HUP signal: " << strsignal(sig) << " – restarting" << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, "caught HUP signal: %s – restarting", strsignal(sig));
    mainThrd.stop();
}

void IntHandler(int sig)
{
    cout << strsignal(sig) << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, "caught INT signal: %s", strsignal(sig));
    mainThrd.stop();
}

int ConfigureSignalHandlers()
{
    int ign_sig[] = {SIGUSR2, SIGPIPE, SIGALRM, SIGTSTP, SIGPROF, SIGCHLD};
    int fatal_sig[] = {SIGQUIT, SIGILL, SIGTRAP, SIGABRT, SIGIOT, SIGBUS, SIGFPE, SIGSEGV, SIGSTKFLT, SIGCONT, SIGPWR, SIGSYS};

    for (int i = 0; i < 6; i++)
        signal(ign_sig[i], SIG_IGN);

    for (int i = 0; i < 12; i++)
    {
        struct sigaction sigact;
        sigact.sa_flags = SA_SIGINFO;
        sigact.sa_sigaction = signal_error;
        sigemptyset(&sigact.sa_mask);
        
        sigaction(fatal_sig[i], &sigact, 0);
    }

    struct sigaction sigtermSA;
    sigtermSA.sa_handler = TermHandler;
    sigemptyset(&sigtermSA.sa_mask);
    sigtermSA.sa_flags = 0;
    sigaction(SIGTERM, &sigtermSA, NULL);

    struct sigaction sigusr1SA;
    sigusr1SA.sa_handler = HupHandler;
    sigemptyset(&sigusr1SA.sa_mask);
    sigusr1SA.sa_flags = 0;
    sigaction(SIGUSR1, &sigusr1SA, NULL);

    struct sigaction sighupSA;
    sighupSA.sa_handler = HupHandler;
    sigemptyset(&sighupSA.sa_mask);
    sighupSA.sa_flags = 0;
    sigaction(SIGHUP, &sighupSA, NULL);

    struct sigaction sigintSA;
    sigintSA.sa_handler = IntHandler;
    sigemptyset(&sigintSA.sa_mask);
    sigintSA.sa_flags = 0;
    sigaction(SIGINT, &sigintSA, NULL);
    
    return 0;
}

void log_error(string str)
{
    cerr << str << endl;
    syslog(LOG_DEBUG | LOG_LOCAL0, str.c_str());
}
