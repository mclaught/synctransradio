/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ChannelUARTWorker.h
 * Author: user
 *
 * Created on 28 января 2020 г., 1:10
 */

#ifndef CHANNELUARTWORKER_H
#define CHANNELUARTWORKER_H

#include "ChannelWorker.h"

class ChannelUARTWorker : public ChannelWorker {
public:
    ChannelUARTWorker();
    ChannelUARTWorker(const ChannelUARTWorker& orig);
    virtual ~ChannelUARTWorker();
    
    virtual bool process();
private:

};

#endif /* CHANNELUARTWORKER_H */

