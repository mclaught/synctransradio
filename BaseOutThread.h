/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BaseOutThread.h
 * Author: user
 *
 * Created on 1 сентября 2020 г., 14:33
 */

#ifndef BASEOUTTHREAD_H
#define BASEOUTTHREAD_H

#include "MyThread.h"
#include "config.h"
#include <mutex>

#pragma pack(push, 1)
typedef struct {
    uint16_t sign;              //сигнатура пакета = 0xAA55
    uint8_t  len;
    uint8_t  slot_cnt;          //кол-во слотов
    uint8_t  slot;              //№ слота (0 - 7)   
    uint16_t bitrate;           //битрейт
//    uint8_t  frame_duration;    //длительность фрейма в мс
    uint8_t channel;
    uint8_t room;
}uart_hdr_t;

typedef struct {
    uart_hdr_t hdr;
    uint8_t  data[PACKET_SIZE+1]; //данные пакета
}uart_packet_t;

typedef struct {
    uint8_t ch1;
}uart_setts_t;
#pragma pack(pop)

enum uart_cmd_t {UART_PAIR=100, UART_PAIR_STOP, UART_SETTS};

#define UART_FIFO_CNT   100
#define UART_FIFO_SIZE  (UART_FIFO_CNT*sizeof(uart_packet_t))

using namespace std;

class BaseOutThread : public MyThread{
public:
    BaseOutThread();
    BaseOutThread(const BaseOutThread& orig);
    virtual ~BaseOutThread();
    
    virtual bool init() = 0;
    void add(int slot, uint8_t *data);
    void pairing(bool start);
    bool process();
    void send(int slot, int room, uint8_t *data);
    void send_setts();
    virtual int write_fd(const void* buf, size_t sz) = 0;
private:
    uint8_t fifo[UART_FIFO_SIZE];
    int fifo_in  = 0;
    int fifo_out = 0;
    mutex mtx;
    int log_cnt = 0;
    uint8_t _crc = 0;
    
protected:
    int fifo_cnt();
    void crc_clear();
    uint8_t crc(uint8_t* data, int len);
    
    virtual void exec();
};

#endif /* BASEOUTTHREAD_H */

