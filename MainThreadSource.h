/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainThreadSource.h
 * Author: user
 *
 * Created on 4 сентября 2020 г., 14:01
 */

#ifndef MAINTHREADSOURCE_H
#define MAINTHREADSOURCE_H

#include <map>
#include <mutex>

#include "Channel.h"
#include "ChannelWorker.h"

class MainThreadSource {
public:
    MainThreadSource();
    MainThreadSource(const MainThreadSource& orig);
    virtual ~MainThreadSource();
    
    void run();
    void stop();
    void init_workers();
    int chnls_count();
private:
    mutex channel_mtx;
    bool active = true;
    ChannelWorker worker[WORKER_THREADS];
    int worker_cnt = 4;
    map<uint32_t, PChannel> channel_map;
    
    PChannel create_channel(uint16_t port, uint16_t room, int wrk);
    PChannel get_channel(uint16_t port, uint16_t room);
};

#endif /* MAINTHREADSOURCE_H */

