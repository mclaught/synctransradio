#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=synctransradio
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/synctransradio
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=synctransradio.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/synctransradio.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release/GNU-Linux
CND_ARTIFACT_NAME_Release=synctransradio
CND_ARTIFACT_PATH_Release=dist/Release/GNU-Linux/synctransradio
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=synctransradio.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/synctransradio.tar
# Release_lan-wan configuration
CND_PLATFORM_Release_lan-wan=GNU-Linux
CND_ARTIFACT_DIR_Release_lan-wan=dist/Release_lan-wan/GNU-Linux
CND_ARTIFACT_NAME_Release_lan-wan=synctransradio
CND_ARTIFACT_PATH_Release_lan-wan=dist/Release_lan-wan/GNU-Linux/synctransradio
CND_PACKAGE_DIR_Release_lan-wan=dist/Release_lan-wan/GNU-Linux/package
CND_PACKAGE_NAME_Release_lan-wan=synctransradio.deb
CND_PACKAGE_PATH_Release_lan-wan=dist/Release_lan-wan/GNU-Linux/package/synctransradio.deb
# Release_wan-uart configuration
CND_PLATFORM_Release_wan-uart=GNU-Linux
CND_ARTIFACT_DIR_Release_wan-uart=dist/Release_wan-uart/GNU-Linux
CND_ARTIFACT_NAME_Release_wan-uart=synctransradio
CND_ARTIFACT_PATH_Release_wan-uart=dist/Release_wan-uart/GNU-Linux/synctransradio
CND_PACKAGE_DIR_Release_wan-uart=dist/Release_wan-uart/GNU-Linux/package
CND_PACKAGE_NAME_Release_wan-uart=synctransradio.tar
CND_PACKAGE_PATH_Release_wan-uart=dist/Release_wan-uart/GNU-Linux/package/synctransradio.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
