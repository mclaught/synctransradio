#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/BaseOutThread.o \
	${OBJECTDIR}/Channel.o \
	${OBJECTDIR}/ChannelUARTWorker.o \
	${OBJECTDIR}/ChannelWorker.o \
	${OBJECTDIR}/MainThreadDest.o \
	${OBJECTDIR}/MainThreadSource.o \
	${OBJECTDIR}/Monitor.o \
	${OBJECTDIR}/MyThread.o \
	${OBJECTDIR}/Settings.o \
	${OBJECTDIR}/UARTThread.o \
	${OBJECTDIR}/UDPOutThread.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread -lopus

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/synctransradio

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/synctransradio: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/synctransradio ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/BaseOutThread.o: BaseOutThread.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BaseOutThread.o BaseOutThread.cpp

${OBJECTDIR}/Channel.o: Channel.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Channel.o Channel.cpp

${OBJECTDIR}/ChannelUARTWorker.o: ChannelUARTWorker.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ChannelUARTWorker.o ChannelUARTWorker.cpp

${OBJECTDIR}/ChannelWorker.o: ChannelWorker.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ChannelWorker.o ChannelWorker.cpp

${OBJECTDIR}/MainThreadDest.o: MainThreadDest.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MainThreadDest.o MainThreadDest.cpp

${OBJECTDIR}/MainThreadSource.o: MainThreadSource.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MainThreadSource.o MainThreadSource.cpp

${OBJECTDIR}/Monitor.o: Monitor.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Monitor.o Monitor.cpp

${OBJECTDIR}/MyThread.o: MyThread.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MyThread.o MyThread.cpp

${OBJECTDIR}/Settings.o: Settings.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Settings.o Settings.cpp

${OBJECTDIR}/UARTThread.o: UARTThread.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/UARTThread.o UARTThread.cpp

${OBJECTDIR}/UDPOutThread.o: UDPOutThread.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/UDPOutThread.o UDPOutThread.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -DIN_CH=0 -DOUT_CH=0 -I/usr/include -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
