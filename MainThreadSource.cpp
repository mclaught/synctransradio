/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainThreadSource.cpp
 * Author: user
 * 
 * Created on 4 сентября 2020 г., 14:01
 */

#include <iostream>
#include <syslog.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "config.h"
#include "MainThreadSource.h"
#include "Monitor.h"
#include "Settings.h"
#include "BaseOutThread.h"
#include "UDPOutThread.h"
#include "UARTThread.h"
#include "myip.h"
#include "MainThread.h"

using namespace std;

MainThreadSource::MainThreadSource() {
}

MainThreadSource::MainThreadSource(const MainThreadSource& orig) {
}

MainThreadSource::~MainThreadSource() {
}

PChannel MainThreadSource::create_channel(uint16_t port, uint16_t room, int wrk){
    PChannel ch = make_shared<Channel>(channel_map.size(), port, room, wrk, &worker[wrk]);//PChannel(new Channel(channel_map.size(), port, room, wrk, &worker[wrk]));
    {
        lock_guard<mutex> lock(channel_mtx);
        channel_map[(room<<16) + port] = ch;
    }

    worker[wrk].add_channel(ch);

    return ch;
}

PChannel MainThreadSource::get_channel(uint16_t port, uint16_t room){
    map<uint32_t, PChannel>::iterator fnd = channel_map.find((room << 16) + port);
    if(fnd == channel_map.end()){
        return PChannel(nullptr);
    }else{
        return fnd->second;
    }
}

void MainThreadSource::init_workers(){
    for(int i=0; i<worker_cnt; i++){
        worker[i].index = i;
        worker[i].terminate();
        worker[i].clear();
    }
    
    channel_map.clear();
    
    monitor.clear();
    
    int wrk = 1;
    auto ports = settings.getChannelsPorts();
    for(int r : settings.rooms){
        for(uint16_t port : ports){
            create_channel(port, r, wrk % worker_cnt);
            wrk++;
        }
    }
    
    for(int i=0; i<worker_cnt; i++){
        cout << "Worker " << i << " cnt=" << worker[i].count() << " complexity=" << worker[i].set_complexity();
        if(i != 0 && worker[i].count()){
            CPU_ZERO(&worker[i].mask);
            CPU_SET(i, &worker[i].mask);        
            worker[i].start();
            cout << " - start" << endl;
        }else if(i == 0){
            cout << " - main thread" << endl;
        }else{
            cout << " - idle" << endl;
        }
    }
}

void print_affinity(const char* thrd) {
    cpu_set_t mask;
    long nproc, i;

    if (sched_getaffinity(0, sizeof(cpu_set_t), &mask) == -1) {
        perror("sched_getaffinity");
    }
    nproc = sysconf(_SC_NPROCESSORS_ONLN);
    printf(thrd);
    printf(" affinity: ");
    for (i = 0; i < nproc; i++) {
        printf("%d ", CPU_ISSET(i, &mask));
    }
    printf("\n");
}    

void MainThreadSource::run(){
    
    int sock = socket(AF_INET, SOCK_RAW, IPPROTO_UDP);
    if(sock == -1)
    {
        cerr << "socket: " << strerror(errno) << endl;
        exit(-1);
    }
    
    int prm = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &prm, sizeof (int)) < 0)
    {
        cerr << "SO_REUSEADDR: " << strerror(errno) << endl;
        exit(-1);
    }

    prm = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &prm, sizeof (int)) < 0)
    {
        cerr << "SO_BROADCAST: " << strerror(errno) << endl;
        exit(-1);
    }

    char* dev = const_cast<char*>(settings.getStr("iface", "eth0").c_str());
    if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, (char*) dev, strlen(dev)) < 0)
    {
        cerr << "SO_BINDTODEVICE: " << strerror(errno) << endl;
        exit(-1);
    }
    cout << "Main socket bound to " << dev << endl;
    
    worker_cnt = sysconf(_SC_NPROCESSORS_ONLN);
    if(worker_cnt < 1)
        worker_cnt = 1;
    if(worker_cnt > WORKER_THREADS)
        worker_cnt = WORKER_THREADS;
    
    bool uart_ok = outThrd.init();
    if(uart_ok){
        cout << "OUT thread started" << endl;
        //uart.start();
    }
    
    init_workers();
    usleep(1000);
    
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(0, &mask);        
    if (sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1) {
        perror("sched_setaffinity");
    }
    print_affinity("main");
    
    while(active)
    {
        fd_set rds, ers;
        timeval tv = {0,1000};
        bool processed = false;
        
        FD_SET(sock, &rds);
        FD_SET(sock, &ers);
        
        int s = select(sock+1, &rds, NULL, &ers, &tv);
        if(s > 0)
        {
            if(FD_ISSET(sock, &rds)){
                //cout << "read packet" << endl;
                uint8_t buf[4096];
                int rd = recv(sock, buf, 1024, 0);
                if(rd == -1)
                {
                    cerr << "recv: " << strerror(errno) << endl;
                }
                else
                {
                    
//                    uint16_t port = ntohs(*(uint16_t*)&buf[22]);
//                    uint16_t cnt = ntohs(*(uint16_t*)&buf[24]);
                    MyIP* ip = (MyIP*)(buf);
                    MyUDP* udp = (MyUDP*)((uint8_t*)ip + ip->IHL*4);
                    uint8_t* data = (uint8_t*)udp + sizeof(MyUDP);
                    int16_t room = *reinterpret_cast<int16_t*>(data);
                    uint16_t from_port = ntohs(udp->src_port);
                    uint16_t to_port = ntohs(udp->dst_port);
                    uint16_t len = ntohs(udp->udp_len) - sizeof(MyUDP);
                    
                    in_addr from, to;
                    from.s_addr = ip->src_ip;
                    to.s_addr = ip->dst_ip;
//                    cout << "recv from " << inet_ntoa(from) << ":" << from_port << " to " << inet_ntoa(to) << ":" << to_port << endl;

                    if(to_port > 1000 && to_port < 1035){
                        PChannel ch = get_channel(to_port, room);
                        if(ch.get()){
                            ch->add(reinterpret_cast<int16_t*>(data), len/2);
                            ch->touch();
                        }
                    }else if(to_port == 1000){
                        for(pair<int, PChannel> ch : channel_map){
                            if(ch.second->room == room && ch.second->port != 1000 && !ch.second->have_stream()){
                                ch.second->add(reinterpret_cast<int16_t*>(data), len/2);
                            }
                        }
                    }else if(to_port == 2000){
                        string str = string(reinterpret_cast<char*>(data), len);
                        
                        sockaddr_in a;
                        memset(&a, 0, sizeof(sockaddr_in));
                        a.sin_family = AF_INET;
                        a.sin_addr = from;
                        a.sin_port = htons(from_port);
                        settings.processSetts(str, a);
                    }
                    
                    processed = true;
                }
            }
            
            if(FD_ISSET(sock, &ers)){
                cerr << "Common socket error" << endl;
            }
        }else if(s < 0)
        {
            cerr << "select: " << strerror(errno) << endl;
        }
        
        for(int i=1; i<worker_cnt; i++){
            if(worker[i].data_ready())
                worker[i].unlock();
        }
        if(worker[0].data_ready())
            worker[0].process();
        
//        uart.process();
        
        monitor.show();
    }
    
    close(sock);
    
    cout << "Stoping..." << endl;
    
    cout << "Stoping OUT thread..." << endl;
    outThrd.terminate();
    
    for(int i=0; i<worker_cnt; i++){
        cout << "Stoping worker " << i << "..." << endl;
        worker[i].terminate();
    }
    cout << "Stoped!" << endl;
}

void MainThreadSource::stop(){
    active = false;
}

int MainThreadSource::chnls_count(){
    return channel_map.size();
}