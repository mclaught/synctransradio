#!/bin/sh
### BEGIN INIT INFO
# Provides:          synctrasnradio
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs $network
# Default-Start:     3 4 5
# Default-Stop:      0 1 6
# Short-Description: Synctrans Radio module
# Description:       Synctrans Radio module
### END INIT INFO

# Author: mclaught <sergey@taldykin.com>
#

DESC="Synctrans Radio module"
NAME=synctransradio
DAEMON=/usr/sbin/${NAME}

case $1 in
    start|restart|force-reload)
        echo "Starting ${DESC}..."

        cd /
        exec > /dev/null
        exec 2> /dev/null
        exec < /dev/null 

        ${DAEMON} stop
        sleep 1
        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            killall ${NAME} -s 9
        fi

        .${DAEMON} start
        ;;

    stop)
        echo "Stoping ${DESC}..."

        ${DAEMON} stop
        sleep 1
        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            killall ${NAME} -s 9
        fi
        ;;

    status)
        pidof ${NAME}
        if [ $? -eq 0 ]
        then
            echo "Running"
            exit 0
        else
            echo "Stoped"
            exit 1
        fi         
	;;
esac


