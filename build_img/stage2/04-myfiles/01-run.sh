#!/bin/bash -e

install -m 755 ${IMG_NAME}/etc/rc.local	"${ROOTFS_DIR}/etc/"
install -m 755 ${IMG_NAME}/usr/sbin/*		"${ROOTFS_DIR}/usr/sbin/"
install -m 755 ${IMG_NAME}/etc/init.d/*.d	"${ROOTFS_DIR}/etc/init.d/"
install -d "${ROOTFS_DIR}/etc/synctransradio"
install -m 755 ${IMG_NAME}/etc/synctransradio/*.*	"${ROOTFS_DIR}/etc/synctransradio/"
install -m 755 ${IMG_NAME}/etc/dhcpcd.conf	"${ROOTFS_DIR}/etc/"

cd ${ROOTFS_DIR}/etc/rc3.d
ln -s ../init.d/synctrans.d S01synctrans.d
cd ${ROOTFS_DIR}/etc/rc4.d
ln -s ../init.d/synctrans.d S01synctrans.d
cd ${ROOTFS_DIR}/etc/rc5.d
ln -s ../init.d/synctrans.d S01synctrans.d
cd ${ROOTFS_DIR}/etc/rc6.d
ln -s ../init.d/synctrans.d S01synctrans.d
    