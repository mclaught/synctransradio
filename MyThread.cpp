/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.cpp
 * Author: user
 * 
 * Created on 16 января 2017 г., 9:56
 */

#include <sys/syslog.h>
#include <fstream>
#include <iostream>
#include <pthread.h>

#include "MyThread.h"

MyThread::MyThread() : active(false)
{
}

MyThread::MyThread(const MyThread& orig)
{
}

MyThread::~MyThread()
{
}

void MyThread::start()
{
    active = true;
    _thrd = thread(_thread_func, this);
}

void MyThread::terminate()
{
    if(active){
        active = false;
        if(_thrd.joinable()){
            _thrd.join();
        }
    }
}

void MyThread::set_prio(int prio){
    pthread_attr_t thAttr;
    int policy = 0;
    int max_prio_for_policy = 0;
    int min_prio_for_policy = 0;
    
    pthread_attr_init(&thAttr);
    pthread_attr_getschedpolicy(&thAttr, &policy);
    max_prio_for_policy = sched_get_priority_max(policy);
    min_prio_for_policy = sched_get_priority_min(policy);
    cout << "max_prio_for_policy=" << max_prio_for_policy << endl;
    cout << "min_prio_for_policy=" << min_prio_for_policy << endl;

    if(prio < max_prio_for_policy)
        prio = max_prio_for_policy;
    if(prio > min_prio_for_policy)
        prio = min_prio_for_policy;

    if(pthread_setschedprio(_thrd.native_handle(), prio)){
        perror("pthread_setschedprio");
    }
    pthread_attr_destroy(&thAttr);
}

void MyThread::_thread_func(MyThread* _this)
{
    _this->exec();
}