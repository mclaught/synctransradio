/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainThreadDest.cpp
 * Author: user
 * 
 * Created on 4 сентября 2020 г., 16:27
 */

#include <iostream>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>

#include "config.h"
#include "BaseOutThread.h"
#include "MainThreadDest.h"
#include "Settings.h"
#include "UDPOutThread.h"
#include "UARTThread.h"
#include "MainThread.h"
#include "Monitor.h"

MainThreadDest::MainThreadDest() {
}

MainThreadDest::MainThreadDest(const MainThreadDest& orig) {
}

MainThreadDest::~MainThreadDest() {
}

int MainThreadDest::createMainSock(){
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1)
    {
        cerr << "socket: " << strerror(errno) << endl;
        exit(-1);
    }
    
    int prm = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &prm, sizeof (int)) < 0)
    {
        cerr << "SO_REUSEADDR: " << strerror(errno) << endl;
        exit(-1);
    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(settings.getInt("remote_port", 40471));
    if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "bind: " << strerror(errno) << endl;
        exit(-1);
    }
    
    if(sock > max_sock)
        max_sock = sock;
    
    cout << "UDP receive thread bound to :" << ntohs(addr.sin_port) << endl;
    
    return sock;
}

int MainThreadDest::createSettSock(){
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1)
    {
        cerr << "socket: " << strerror(errno) << endl;
        exit(-1);
    }
    
    int prm = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &prm, sizeof (int)) < 0)
    {
        cerr << "SO_REUSEADDR: " << strerror(errno) << endl;
        exit(-1);
    }
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(2000);
    if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "bind: " << strerror(errno) << endl;
        exit(-1);
    }
    
    if(sock > max_sock)
        max_sock = sock;
    
    cout << "UDP settings bound to :" << ntohs(addr.sin_port) << endl;
    
    return sock;
}

uint32_t getTick() {
    struct timespec ts;
    unsigned theTick = 0U;
    clock_gettime( CLOCK_REALTIME, &ts );
    theTick  = ts.tv_nsec / 1000000;
    theTick += ts.tv_sec * 1000;
    return theTick;
}

void MainThreadDest::stunSend(){
    if(stun_addr == "" || stun_id == 0)//
        return;
    
    uint32_t tm = getTick();
    if(tm < (last_stun_tm + 1000))
        return;
    last_stun_tm = tm;
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(stun_addr.c_str());
    addr.sin_port = htons(remote_port);
    
    if(sendto(sock, &stun_id, sizeof(uint16_t), 0, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "stun sendto: " << strerror(errno) << endl;
    }
}

void MainThreadDest::run(){
    stun_addr = settings.getStr("remote_addr", "");
    remote_port = settings.getInt("remote_port", 40471);
    stun_id = settings.getInt("session", 0);
    if(stun_id){
        cout << "Session: " << stun_id << " remote_addr: " << stun_addr << ":" << remote_port << endl;
    }
    
    sock = createMainSock();
    sockS = createSettSock();
    
    bool uart_ok = outThrd.init();
    if(uart_ok){
        cout << "OUT thread started" << endl;
        //uart.start();
    }
    
    fd_set rd_set;
    timeval tv = {1,0};
    char buf[1500];
    sockaddr_in from;
    socklen_t from_len = sizeof(sockaddr_in);
    memset(&from, 0, sizeof(sockaddr_in));
    
    while(active){
        FD_ZERO(&rd_set);
        FD_SET(sock, &rd_set);
        FD_SET(sockS, &rd_set);

//        for(int i=0; i<29; i++)
//            buf[i] = 0xAA;
//        outThrd.write_fd(buf, 29);
//        usleep(10000);
        
        int cnt = select(max_sock+1, &rd_set, nullptr, nullptr, &tv);
        if(cnt == -1){
            cerr << "select: " << strerror(errno) << endl;
        }else if(cnt > 0){
            if(FD_ISSET(sock, &rd_set)){
                int rd = recvfrom(sock, buf, 1500, 0, reinterpret_cast<sockaddr*>(&from), &from_len);
                if(rd == -1){
                    cerr << "recvfrom: " << strerror(errno) << endl;
                }else if(rd > 0){
                    *reinterpret_cast<uint16_t*>(buf) = 0xAA55;
                    outThrd.write_fd(buf, rd);
                }
            }
            if(FD_ISSET(sockS, &rd_set)){
                int rd = recvfrom(sockS, buf, 1500, 0, reinterpret_cast<sockaddr*>(&from), &from_len);
                if(rd == -1){
                    cerr << "recvfrom sockS: " << strerror(errno) << endl;
                }else if(rd > 0){
                    settings.processSetts(string(buf, rd), from);
                }
            }
        }
        
        monitor.show();
        
        stunSend();
    }
    
    close(sock);
    
    cout << "Stoping..." << endl;
    
    cout << "Stoping OUT thread..." << endl;
    outThrd.terminate();
    cout << "Stoped!" << endl;
}

void MainThreadDest::stop(){
    active = false;
}

void MainThreadDest::init_workers(){
}
