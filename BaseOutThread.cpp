/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BaseOutThread.cpp
 * Author: user
 * 
 * Created on 1 сентября 2020 г., 14:33
 */

#include "BaseOutThread.h"
#include "Channel.h"
#include "Settings.h"
#include "Monitor.h"
#include "MainThread.h"

#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <map>
#include <sched.h>

extern mutex channel_mtx;
//extern map<uint16_t, PChannel> channel_map;

void print_affinity(const char* thrd);

BaseOutThread::BaseOutThread() : MyThread() {
}

BaseOutThread::BaseOutThread(const BaseOutThread& orig) {
}

BaseOutThread::~BaseOutThread() {
}

void BaseOutThread::add(int slot, uint8_t *data){
/*
    if((UART_FIFO_SIZE - fifo_cnt()) < sizeof(uart_packet_t))
        return;
    
    uart_packet_t* pack = reinterpret_cast<uart_packet_t*>(&fifo[fifo_in]);
    
    int packet_size = settings.bitrate * FRAME_DURATION / 8000;
    
    pack->hdr.sign = 0xAA55;
    {
        lock_guard<mutex> lock(channel_mtx);
        pack->hdr.slot_cnt = slot < UART_PAIR ? channel_map.size() : 0;
    }
    pack->hdr.len = sizeof(uart_hdr_t) + packet_size + 1;
    pack->hdr.bitrate = settings.bitrate;
    pack->hdr.slot = slot;
    pack->hdr.channel = settings.channel;
//    pack->hdr.bitrate = BITRATE;
//    pack->hdr.frame_duration = FRAME_DURATION;
    if(data){
        memcpy(pack->data, data, PACKET_SIZE);
    }else{
        memset(pack->data, 0, PACKET_SIZE);
    }
    crc_clear();
    pack->data[packet_size] = crc(reinterpret_cast<uint8_t*>(pack), pack->hdr.len-1);
    
    lock_guard<mutex> lock(mtx);
    
    fifo_in += sizeof(uart_packet_t);
    if(fifo_in >= UART_FIFO_SIZE)
        fifo_in = 0;
 */
}

void BaseOutThread::pairing(bool start){
    cout << "PAIRING " << start << endl;
    send(start ? UART_PAIR : UART_PAIR_STOP, 0, nullptr);
}

bool BaseOutThread::process(){
    /*
    if(fifo_cnt() >= sizeof(uart_packet_t)){

        uint8_t* pack = reinterpret_cast<uint8_t*>(&fifo[fifo_out]);

        if(write(fd, pack, sizeof(uart_packet_t)) == -1){
            cerr << "UARTThread write: " << strerror(errno) << endl;
        }
        monitor.add_uart_traf(sizeof(uart_packet_t)*8);

        {
            lock_guard<mutex> lock(mtx);

            fifo_out += sizeof(uart_packet_t);
            if(fifo_out >= UART_FIFO_SIZE)
                fifo_out = 0;
        }

        monitor.add_uart(fifo_cnt());
//        log_cnt++;
//        if(log_cnt == 100){
//            cout << "UART buf " << fifo_cnt() << endl;
//            log_cnt = 0;
//        }
        return true;
    }else{
        return false;
    }
    */
    return false;
}

void BaseOutThread::send(int slot, int room, uint8_t *data){
    lock_guard<mutex> lock(mtx);
    
    uart_packet_t pack;
    int packet_size = settings.bitrate * FRAME_DURATION / 8000;
    
    pack.hdr.sign = 0xAA55;
    pack.hdr.len = sizeof(uart_hdr_t) + packet_size + 1;
    pack.hdr.bitrate = settings.bitrate;
    pack.hdr.slot_cnt = slot < UART_PAIR ? mainThrd.chnls_count() : 0;
    pack.hdr.slot = slot;
    pack.hdr.room = room;
    pack.hdr.channel = settings.channel;
//    pack.hdr.bitrate = BITRATE;
//    pack.hdr.frame_duration = FRAME_DURATION;
    if(data){
        memcpy(pack.data, data, PACKET_SIZE);
    }else{
        memset(pack.data, 0, PACKET_SIZE);
    }
    crc_clear();
    pack.data[packet_size] = crc(reinterpret_cast<uint8_t*>(&pack), pack.hdr.len-1);
    
    write_fd(&pack, pack.hdr.len);
    
//    monitor.add_uart_traf(pack.hdr.len*8);
}

void BaseOutThread::send_setts(){
    cout << "SEND SETTS " << endl;
    
    uart_setts_t setts;
    setts.ch1 = settings.getInt("channel", 0);
    
    send(UART_SETTS, 0, reinterpret_cast<uint8_t*>(&setts));
}

void BaseOutThread::crc_clear(){
    _crc = 0;
}

uint8_t BaseOutThread::crc(uint8_t* data, int len){
    for(int i=0; i<len; i++)
        _crc ^= data[i];
    return _crc;
}

int BaseOutThread::fifo_cnt(){
    lock_guard<mutex> lock(mtx);
    
    if(fifo_in >= fifo_out)
        return fifo_in - fifo_out;
    else
        return fifo_in + UART_FIFO_SIZE - fifo_out;
}

void BaseOutThread::exec(){
    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(1, &mask);        
    if (sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1) {
        perror("sched_setaffinity");
    }
    print_affinity("uart");
    
    while(active){
        if(!process()){
            usleep(100);
        }
    }
    cout << "UART stoped" << endl;
}
