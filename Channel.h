/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Channel.h
 * Author: user
 *
 * Created on 16 января 2020 г., 14:30
 */

#ifndef CHANNEL_H
#define CHANNEL_H

#include "config.h"

#include <opus/opus.h>
#include <mutex>
#include <memory>

#define FIFO_FRAMES    100
#define FIFO_SIZE   (FRAME_SIZE_IN*FIFO_FRAMES)

using namespace std;

class ChannelWorker;
class Channel {
public:
    Channel(int _slot, uint16_t _port, uint16_t _room, int _thread_n, ChannelWorker* _worker);
    Channel(const Channel& orig);
    virtual ~Channel();
    
    uint16_t room;
    uint16_t port;
    
    int add(int16_t *data, int cnt);
    bool data_ready();
    bool process();
    void touch();
    bool have_stream();
    void set_complexity(int c);
private:
    mutex mtx;
    OpusEncoder* encoder; 
    int slot;
    int thread_n;
    ChannelWorker* worker;
    uint32_t last_tm = 0;
    uint32_t log_tm = 0;
//    uint32_t tps = 1000;
//    uint32_t mspc = 10;
//    int log_cnt = 0;
    
    opus_int16 fifo[FIFO_SIZE];
    int fifo_in = 0;
    int fifo_out = 0;
    int fifo_cnt();
    
    bool create_opus();
    void resample_to(int16_t *data);
    void send_uart(uint8_t* pack);
    int16_t sin_frame(int x);
};

typedef shared_ptr<Channel> PChannel;

#endif /* CHANNEL_H */

