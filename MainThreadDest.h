/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MainThreadDest.h
 * Author: user
 *
 * Created on 4 сентября 2020 г., 16:27
 */

#ifndef MAINTHREADDEST_H
#define MAINTHREADDEST_H

#include <map>

using namespace std;

class MainThreadDest {
public:
    MainThreadDest();
    MainThreadDest(const MainThreadDest& orig);
    virtual ~MainThreadDest();
    
    void run();
    void stop();
    void init_workers();
    int chnls_count(){return 0;};
    int createMainSock();
    int createSettSock();
private:
    bool active = true;
    int sock, sockS;
    int max_sock = 0;
    string stun_addr;
    uint16_t remote_port;
    uint16_t stun_id;
    uint32_t last_stun_tm;
    
    void stunSend();
};

#endif /* MAINTHREADDEST_H */

