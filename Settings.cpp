/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Settings.cpp
 * Author: user
 * 
 * Created on 24 января 2020 г., 14:37
 */

#include "Settings.h"
#include "BaseOutThread.h"
#include "config.h"
#include "MainThreadSource.h"
#include "MainThread.h"

#include <fstream>
#include <string>
#include <string.h>
#include <strstream>
#include <sys/types.h>
#include <sys/socket.h>
#include <iomanip>
#include <iostream>

Settings settings;

Settings::Settings() {
    load();
    
    udp_sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (udp_sock < 0)
        throw runtime_error("socket: " + string(strerror(errno)));

    int prm = 1;
    if (setsockopt(udp_sock, SOL_SOCKET, SO_REUSEADDR, &prm, sizeof (int)) < 0)
        throw runtime_error("setsockopt: " + string(strerror(errno)));

    prm = 1;
    if (setsockopt(udp_sock, SOL_SOCKET, SO_BROADCAST, &prm, sizeof (int)) < 0)
        throw runtime_error("setsockopt: " + string(strerror(errno)));

    char* dev = const_cast<char*>(getStr("iface", "eth0").c_str());
    if (setsockopt(udp_sock, SOL_SOCKET, SO_BINDTODEVICE, (char*) dev, strlen(dev)) < 0)
        throw runtime_error("setsockopt: " + string(strerror(errno)));

    prm = 1;
    if (setsockopt(udp_sock, SOL_SOCKET, SO_NO_CHECK, &prm, sizeof (int)) < 0)
        throw runtime_error("setsockopt: " + string(strerror(errno)));

    sockaddr_in addr;
    memset(&addr, 0, sizeof (sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    //    inet_aton("172.16.10.1", &addr.sin_addr);
    addr.sin_port = htons(2001);
    if (bind(udp_sock, (sockaddr*) & addr, sizeof (sockaddr_in)) < 0)
        throw runtime_error("bind: " + string(strerror(errno)));
    
}

Settings::Settings(const Settings& orig) {
}

Settings::~Settings() {
}

void Settings::setQuickParams(){
    channel = getInt("channel", 0);
    bitrate = getInt("bitrate", 24000);
    
    rooms.clear();
    if(js.contains("rooms") && js["rooms"].is_array()){
        for(auto r : js["rooms"]){
            rooms.push_back(r);
        }
    }
    if(rooms.empty()){
        rooms.push_back(0);
    }
}

void Settings::load(){
    string name = string(SETTS_PATH)+"/settings.json";

    ifstream sett_file;
    sett_file.open(name);
    if(sett_file.is_open()){
        try{
            js = json::parse(sett_file, nullptr, false);
            setQuickParams();
        }catch(json::exception& e){
            cerr << "settings load: " << e.what() << endl;
        }
    }
    
    loadNetParams(true);
}

void Settings::save(){
    string cmd = string("mkdir -p ")+SETTS_PATH;
    system(cmd.c_str());
    
    try{
        string name = string(SETTS_PATH)+"/settings.json";

        ofstream file;
        file.open(name);
        if(file.is_open()){
            file << setw(2) << setfill(' ') << js;
            file.close();
        }
    }catch(json::exception& e){
        cerr << e.what() << endl;
    }
    
    saveNetParams();
}

void Settings::init(){
    if(!js.is_object()){
        js = json::object();
    }
}

void Settings::processSetts(string str, sockaddr_in addr){
    cout << "config: " << str << endl;
    
    if(str.substr(0,8) == "GETSETTS"){
        sendJSONText(addr);
    }else if(str.substr(0,8) == "SETSETTS"){
        str = str.erase(0,8);
        setJSONText(str, addr);
        mainThrd.init_workers();
        outThrd.send_setts();
    }else if(str.substr(0,9) == "DISCOVERY"){
        discovery(addr);
    }else if(str.substr(0,13) == "PAIRING_START"){
        outThrd.pairing(true);
    }else if(str.substr(0,12) == "PAIRING_STOP"){
        outThrd.pairing(false);
    }else if(str.substr(0,12) == "SETTSTOUART"){
        outThrd.send_setts();
    }
}

int Settings::getInt(string name, int def){
    if(js.is_object() && js.contains(name) && js[name].is_number()){
        return js[name];
    }else{
        return def;
    }
}

void Settings::setInt(string name, int val){
    init();
    
    js[name] = val;
}

string Settings::getStr(string name, string def){
    if(js.is_object() && js.contains(name) && js[name].is_string()){
        return js[name];
    }else{
        return def;
    }
}

void Settings::setStr(string name, string val){
    init();
    
    js[name] = val;
}

string Settings::getJSONText(){
    init();
    
    return js.dump();
}

string Settings::setJSONText(string str){
    try{
        js = json::parse(str);
        setQuickParams();
        save();
        return "";
    }catch(json::exception& e){
        cerr << "JSON PARSE: " << e.what() << endl;
        return e.what();
    }
}

void Settings::setJSONText(string str, sockaddr_in addr){
    string error = setJSONText(str);
    string ans = error=="" ? "SETTS_SAVED" : "SETTS_ERROR "+error;
    if(sendto(udp_sock, ans.c_str(), ans.length(), 0, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "sendto: " << strerror(errno) << endl;
    }
}

void Settings::sendJSONText(sockaddr_in addr){
    string str = getJSONText();
    
    cout << "send settings: " << str << endl;
    
    if(sendto(udp_sock, str.c_str(), str.length(), 0, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "sendto: " << strerror(errno) << endl;
    }
}

void Settings::discovery(sockaddr_in addr){
    string str = "IAMHERE";
    if(sendto(udp_sock, str.c_str(), str.length(), 0, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "sendto: " << strerror(errno) << endl;
    }
}

vector<uint16_t> Settings::getChannelsPorts(){
    init();
    
    vector<uint16_t> ports;
    
    if(js.contains("langs") && js["langs"].is_array()){
        for(json j : js["langs"]){
            if(ports.size() < MAX_CHANNELS && j.is_object() && j.contains("port"))
                ports.push_back(j["port"]);
        }
    }
    
    return ports;
}

vector<string> Settings::loadNetParams(bool read){
    vector<string> strs;
    ifstream f;
    f.open("/etc/dhcpcd.conf");
    if(f.good()){
        if(!js.contains("network")){
            js["network"] = json::object();
        }
        
        string s;
        while(!f.eof()){
            getline(f, s, '\n');
            if(read && s.find("static ip_address=") == 0){
                js["network"]["ip"] = s.substr(18);
            }
            if(read && s.find("static routers=") == 0){
                js["network"]["gate"] = s.substr(15);
            }
            strs.push_back(s);
        }
    }
    
    while(strs.back().empty())
        strs.pop_back();
    
    return strs;
}

void Settings::saveNetParams(){
    auto strs = loadNetParams(false);
    ofstream f;
    f.open("/etc/dhcpcd.conf");
    if(f.good()){
        for(auto s : strs){
            if(s.find("static") != 0 && s.find("interface") != 0){
                f << s << endl;
            }
        }
        
        cout << js.dump(4);
        
        if(js.contains("network") && js["network"].contains("ip")){
            f << endl;
            f << "interface " << getStr("iface", "eth0") << endl;
            
            string ip = js["network"]["ip"];
            f << "static ip_address=" << ip << endl;
            
            if(js["network"].contains("gate")){
                string gate = js["network"]["gate"];
                f << "static routers=" << gate << endl;
            }
            f << "static domain_name_servers=8.8.8.8 8.8.4.4" << endl;
        }
        
        f.close();
    }
}