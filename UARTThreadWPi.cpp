/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThreadWPi.cpp
 * Author: user
 * 
 * Created on 8 сентября 2020 г., 12:57
 */
#include <wiringSerial.h>
#include <iostream>
#include <unistd.h>

#include "UARTThreadWPi.h"
#include "Settings.h"

using namespace std;

UARTThreadWPi::UARTThreadWPi() : BaseOutThread() {
}

UARTThreadWPi::UARTThreadWPi(const UARTThreadWPi& orig) {
}

UARTThreadWPi::~UARTThreadWPi() {
}

bool UARTThreadWPi::init(){
    string portname = settings.getStr("uart_port", "/dev/ttyAMA0");
    cout  << "wiringSerial port " << portname << "...";
    
    fd = serialOpen(portname.c_str(), 460800);
    if(fd == -1){
        cerr << "Error " << errno << " opening " << portname << " " << strerror (errno) << endl;
        return false;
    }
    cout  << "460800...opened" << endl;
    
    return true;
}

int UARTThreadWPi::write_fd(const void* buf, size_t sz){
    int wr = write(fd, buf, sz);
    if(wr == -1){
        cerr << "UARTThread write: " << strerror(errno) << endl;
        return -1;
    }
    return wr;
    
//    const char* str = reinterpret_cast<const char*>(buf);
//    for(int i=0; i<sz; i++){
//        serialPutchar(fd, str[i]);
//    }
//    serialFlush(fd);
//    return sz;
}