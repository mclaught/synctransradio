/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Monitor.h
 * Author: user
 *
 * Created on 28 января 2020 г., 0:24
 */

#ifndef MONITOR_H
#define MONITOR_H

#include <stdint.h>
#include <mutex>
#include <sys/time.h>

using namespace std;

class Monitor {
public:
    Monitor();
    Monitor(const Monitor& orig);
    virtual ~Monitor();
    
    void clear();
    void add_opus(int v);
    void add_uart(int v);
    void add_net_traf(int v);
    void add_uart_traf(int v);
    void set_opus_tm();
    void set_sound(int16_t v);
    void show();
    
    uint32_t get_tick_count();
    void pinpoint();
    uint32_t delay_us();
private:
    int opus_sum = 0;
    int opus_n = 0;
    int uart_max;
    int net_traf;
    int uart_traf;
    uint32_t opus_tm;
    uint32_t opus_tm_n;
    uint32_t mspc;
    uint32_t last_tm = 0;
    int16_t sound_max = 0;
    int16_t sound_min = 32000;
    mutex mtx;
    timeval tv_start;
};

extern Monitor monitor;

#endif /* MONITOR_H */

