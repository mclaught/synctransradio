/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDPOutThread.h
 * Author: user
 *
 * Created on 2 сентября 2020 г., 12:42
 */

#ifndef UDPOUTTHREAD_H
#define UDPOUTTHREAD_H

#include <netinet/in.h>

#include "BaseOutThread.h"


class UDPOutThread : public BaseOutThread {
public:
    UDPOutThread();
    UDPOutThread(const UDPOutThread& orig);
    virtual ~UDPOutThread();
    
    virtual bool init();
    virtual int write_fd(const void* buf, size_t sz);
private:
    int sock;
    uint16_t id;
    sockaddr_in addr_dst;
    
protected:
};

#endif /* UDPOUTTHREAD_H */

