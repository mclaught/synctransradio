/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Settings.h
 * Author: user
 *
 * Created on 24 января 2020 г., 14:37
 */

#ifndef SETTINGS_H
#define SETTINGS_H

#include "json.hpp"

#include <netinet/in.h>
#include <vector>

#define SETTS_PATH  "/etc/synctransradio"

using namespace std;
using namespace nlohmann;

class Settings {
public:
    Settings();
    Settings(const Settings& orig);
    virtual ~Settings();
    
    uint8_t channel = 0;
    int bitrate = 24000;
    vector<int> rooms;
    
    void processSetts(string str, sockaddr_in addr);
    
    int getInt(string name, int def);
    void setInt(string name, int val);
    string getStr(string name, string def);
    void setStr(string name, string val);
    
    vector<uint16_t> getChannelsPorts();
    
    string getJSONText();
    string setJSONText(string str);
    void setJSONText(string str, sockaddr_in addr);
    void sendJSONText(sockaddr_in addr);
    void discovery(sockaddr_in addr);
private:
    json js;
    bool loaded = false;
    int udp_sock;
    
    void init();
    void load();
    void save();
    void setQuickParams();
    vector<string> loadNetParams(bool read);
    void saveNetParams();
};

extern Settings settings;

#endif /* SETTINGS_H */

