/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MyThread.h
 * Author: user
 *
 * Created on 16 января 2017 г., 9:56
 */

#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <thread>

using namespace std;

class MyThread {
private:
    thread _thrd;
    static void _thread_func(MyThread* _this);
public:
    bool active;
    
    MyThread();
    MyThread(const MyThread& orig);
    virtual ~MyThread();
    virtual void start();
    virtual void terminate();
    void set_prio(int prio);
    
    virtual void exec() = 0;
};

#endif /* MYTHREAD_H */

