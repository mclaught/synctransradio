/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Channel.cpp
 * Author: user
 * 
 * Created on 16 января 2020 г., 14:30
 */
#include <string.h>
#include <iostream>
#include <sys/time.h>
#include <sys/times.h>
#include <unistd.h>
#include <math.h>

#include "Channel.h"
#include "Monitor.h"
#include "ChannelWorker.h"
#include "Settings.h"
#include "BaseOutThread.h"
#include "MainThread.h"

Channel::Channel(int _slot, uint16_t _port, uint16_t _room, int _thread_n, ChannelWorker* _worker) : slot(_slot), port(_port), room(_room), thread_n(_thread_n), worker(_worker) {
    printf("Channel %d(%d/%d) -> %d\r\n", _slot, _port, _room, _thread_n);
    create_opus();
//    tps = sysconf(_SC_CLK_TCK);
//    if(tps)
//        mspc = 1000/tps;
//    cout << "Clock ticks " << tps << endl;
}

Channel::Channel(const Channel& orig) {
}

Channel::~Channel() {
    printf("Delete channel %d(%d) -> %d\r\n", slot, port, thread_n);
}

int Channel::fifo_cnt(){
    lock_guard<mutex> lock(mtx);
    
    if(fifo_in >= fifo_out)
        return fifo_in - fifo_out;
    else
        return fifo_in + FIFO_SIZE - fifo_out;
}

bool Channel::create_opus()
{
    int error;
    encoder = opus_encoder_create(SAMPLE_RATE_OPUS, CHANNELS, OPUS_APPLICATION_AUDIO, &error);
    if (error < 0)
    {
        throw runtime_error("opus_encoder_create: " + string(opus_strerror(error)));
    }

    error = opus_encoder_ctl(encoder, OPUS_SET_VBR(0));
    if(error < 0)
    {
        throw runtime_error("opus_encoder_ctl OPUS_SET_VBR: " + string(opus_strerror(error)));
    }

    error = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(settings.bitrate));
    if(error < 0)
    {
        throw runtime_error("opus_encoder_ctl OPUS_SET_BITRATE: " + string(opus_strerror(error)));
    }

    error = opus_encoder_ctl(encoder, OPUS_SET_SIGNAL(OPUS_SIGNAL_MUSIC));
    if(error < 0)
    {
        throw runtime_error("opus_encoder_ctl OPUS_SET_SIGNAL: " + string(opus_strerror(error)));
    }
//
//    error = opus_encoder_ctl(encoder, OPUS_SET_BANDWIDTH(OPUS_BANDWIDTH_FULLBAND));
//    if(error < 0)
//    {
//        throw runtime_error("opus_encoder_ctl OPUS_SET_BANDWIDTH: " + string(opus_strerror(error)));
//    }

    error = opus_encoder_ctl(encoder, OPUS_SET_COMPLEXITY(10));
    if(error < 0)
    {
        throw runtime_error("opus_encoder_ctl OPUS_SET_COMPLEXITY: " + string(opus_strerror(error)));
    }

    error = opus_encoder_ctl(encoder, OPUS_SET_EXPERT_FRAME_DURATION(OPUS_FRAMESIZE));
    if(error < 0)
    {
        throw runtime_error("opus_encoder_ctl OPUS_SET_EXPERT_FRAME_DURATION: " + string(opus_strerror(error)));
    }

    return true;
}

void Channel::set_complexity(int c){
//    int error = opus_encoder_ctl(encoder, OPUS_SET_COMPLEXITY(10));
//    if(error < 0)
//    {
//        cerr << opus_strerror(error) << endl;
//    }
}

int Channel::add(int16_t *data, int cnt){
    data++;
    cnt--;
    
    if((FIFO_SIZE-fifo_cnt()) < cnt)
        return fifo_cnt();
    
    //monitor.add_net_traf(cnt*2);
    
    int tail = FIFO_SIZE - fifo_in;
    int min = std::min(tail, cnt);
    memcpy(&fifo[fifo_in], data, min*sizeof(opus_int16));
    
    cnt -= min;
    if(cnt > 0){
        memcpy(&fifo[0], &data[min], cnt*sizeof(opus_int16));
        
        lock_guard<mutex> lock(mtx);
        fifo_in = cnt;
    }else{
        lock_guard<mutex> lock(mtx);
        fifo_in += min;
        if(fifo_in >= FIFO_SIZE)
            fifo_in = 0;
    }
    
    return fifo_cnt();
}

bool Channel::data_ready(){
    return fifo_cnt() >= FRAME_SIZE_IN;
}

int16_t Channel::sin_frame(int x){
    return sin((double)x/(double)FRAME_SIZE_OPUS*10)*1000;
}

void Channel::resample_to(int16_t *data){
    for(int i=0; i<FRAME_SIZE_OPUS; i++){
        int j = i*SAMPLE_RATE_IN/SAMPLE_RATE_OPUS;
        if(j>=FRAME_SIZE_IN){
            j = (FRAME_SIZE_IN-1);
        }
        data[i] = fifo[fifo_out + j];//sin_frame(i);//
        monitor.set_sound(data[i]);
    }
    
    lock_guard<mutex> lock(mtx);
    
    fifo_out += FRAME_SIZE_IN;
    if(fifo_out >= FIFO_SIZE)
        fifo_out = 0;
}

bool Channel::process(){
    if(data_ready()){
        //monitor.pinpoint();
        
        int16_t data[FRAME_SIZE_OPUS];
        uint8_t opus[PACKET_SIZE];

        resample_to(data);
        //monitor.add_opus(fifo_cnt()*FRAME_DURATION/FRAME_SIZE_IN);

        uint32_t tm = monitor.get_tick_count();
        if((tm - log_tm) >= 1000){
    //        cout << slot << "\t" << thread_n << "\t" << fifo_cnt()*20/FRAME_SIZE_IN << endl;
            log_tm = tm;
        }

        int packet_size = settings.bitrate * FRAME_DURATION / 8000;
        opus_int32 error = opus_encode(encoder, data, FRAME_SIZE_OPUS, opus, packet_size);
        if(error < 0){
            cerr << "OPUS error: " << error << endl;
            return true;
        }

        send_uart(opus);
        
        //monitor.set_opus_tm();
        
        return true;
    }else{
        return false;
    }
}

void Channel::send_uart(uint8_t* pack){
//    uart.add(slot, pack);
    outThrd.send(slot, room, pack);
}

void Channel::touch(){
//    timeval tv;
//    gettimeofday(&tv, 0);
    last_tm = monitor.get_tick_count();//times(&tm)*mspc;//tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

bool Channel::have_stream(){
//    timeval tv;
//    gettimeofday(&tv, 0);
//    uint64_t tm = tv.tv_sec * 1000 + tv.tv_usec / 1000;
    uint32_t cur_tm = monitor.get_tick_count();;//tv.tv_sec * 1000 + tv.tv_usec / 1000;
    return (cur_tm - last_tm) < 100;
}