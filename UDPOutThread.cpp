/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UDPOutThread.cpp
 * Author: user
 * 
 * Created on 2 сентября 2020 г., 12:42
 */

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <iostream>

#include "UDPOutThread.h"
#include "Settings.h"
#include "Monitor.h"

UDPOutThread::UDPOutThread() : BaseOutThread() {
}

UDPOutThread::UDPOutThread(const UDPOutThread& orig) {
}

UDPOutThread::~UDPOutThread() {
}

bool UDPOutThread::init(){
    cout << "UDPOutThread...";
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock == -1)
    {
        cerr << "socket: " << strerror(errno) << endl;
        exit(-1);
    }
    
    int prm = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &prm, sizeof (int)) < 0)
    {
        cerr << "SO_REUSEADDR: " << strerror(errno) << endl;
        exit(-1);
    }

    char* dev = const_cast<char*>(settings.getStr("remote_iface", "eth0").c_str());
    if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, (char*) dev, strlen(dev)) < 0)
    {
        cerr << "SO_BINDTODEVICE: " << strerror(errno) << endl;
        exit(-1);
    }
    cout << "bound to " << dev << "...";
    
    sockaddr_in addr;
    memset(&addr, 0, sizeof(sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = 0;
    if(bind(sock, reinterpret_cast<sockaddr*>(&addr), sizeof(sockaddr_in)) == -1){
        cerr << "UDPOutThread bind: " << strerror(errno) << endl;
        exit(-1);
    }
    
    string dst_ip = settings.getStr("remote_addr", "");
    int dst_port = settings.getInt("remote_port", 0);
    
    memset(&addr_dst, 0, sizeof(sockaddr_in));
    addr_dst.sin_family = AF_INET;
    addr_dst.sin_addr.s_addr = inet_addr(dst_ip.c_str());
    addr_dst.sin_port = htons(dst_port);
    
    id = settings.getInt("session", 0);
    
    cout << "remote addr: " << dst_ip << ":" << dst_port << " session: " << id << endl;
    
    return true;
}

int UDPOutThread::write_fd(const void* buf, size_t sz){
    *reinterpret_cast<uint16_t*>(const_cast<void*>(buf)) = id | 0x8000;
    
    int wr = sendto(sock, buf, sz, 0, reinterpret_cast<sockaddr*>(&addr_dst), sizeof(sockaddr_in));
    if(wr == -1){
        cerr << "UDPOutThread sendto: " << strerror(errno) << endl;
        return -1;
    }
    monitor.add_uart_traf(wr*8);
    return wr;
}