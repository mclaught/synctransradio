/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ChannelWorker.h
 * Author: user
 *
 * Created on 16 января 2020 г., 18:37
 */

#ifndef CHANNELWORKER_H
#define CHANNELWORKER_H

#include "MyThread.h"
#include "Channel.h"

#include <set>
#include <mutex>
#include <condition_variable>
#include <sched.h>

using namespace std;

typedef set<PChannel> channels_t;

class ChannelWorker : public MyThread {
public:
    ChannelWorker();
    ChannelWorker(const ChannelWorker& orig);
    virtual ~ChannelWorker();
    
    int index;
    cpu_set_t mask;
    
    virtual void start();
    virtual void terminate();
    void clear();
    void add_channel(PChannel ch);
    int count();
    int set_complexity();
    bool data_ready();
    void unlock();
    virtual bool process();
private:
    channels_t channels;
    mutex mtx;
    mutex sync_mtx;
    condition_variable sync;
    
    virtual void exec();
};

#endif /* CHANNELWORKER_H */

