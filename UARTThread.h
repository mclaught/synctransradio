/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThread.h
 * Author: user
 *
 * Created on 17 января 2020 г., 14:07
 */

#ifndef UARTTHREAD_H
#define UARTTHREAD_H

#include "MyThread.h"
#include "BaseOutThread.h"

using namespace std;

class UARTThread : public BaseOutThread {
public:
    int fd;
    
    UARTThread();
    UARTThread(const UARTThread& orig);
    virtual ~UARTThread();
    
    virtual bool init();
    virtual int write_fd(const void* buf, size_t sz);
private:
    bool set_interface_attribs (int fd, int speed, int parity);
    bool set_blocking (int fd, int should_block);
    
protected:
};

#endif /* UARTTHREAD_H */

