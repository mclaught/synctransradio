/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ChannelUARTWorker.cpp
 * Author: user
 * 
 * Created on 28 января 2020 г., 1:10
 */

#include "ChannelUARTWorker.h"
#include "BaseOutThread.h"
#include "MainThread.h"

ChannelUARTWorker::ChannelUARTWorker() : ChannelWorker() {
}

ChannelUARTWorker::ChannelUARTWorker(const ChannelUARTWorker& orig) {
}

ChannelUARTWorker::~ChannelUARTWorker() {
}

bool ChannelUARTWorker::process(){
    ChannelWorker::process();
    outThrd.process();
    return true;    
}