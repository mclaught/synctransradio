/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UARTThreadWPi.h
 * Author: user
 *
 * Created on 8 сентября 2020 г., 12:57
 */

#ifndef UARTTHREADWPI_H
#define UARTTHREADWPI_H

#include "BaseOutThread.h"


class UARTThreadWPi : public BaseOutThread {
public:
    UARTThreadWPi();
    UARTThreadWPi(const UARTThreadWPi& orig);
    virtual ~UARTThreadWPi();
    
    int fd;
    
    virtual bool init();
    virtual int write_fd(const void* buf, size_t sz);
private:

};

#endif /* UARTTHREADWPI_H */

